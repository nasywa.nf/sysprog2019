Untuk mengeksekusi script_project.sh, cukup menjalankan "bash script_project.sh".
Script akan secara otomatis melakukan instalasi dependensi yang dibutuhkan dan menampilkan pilihan menu yang dapat digunakan.
Menu 1 untuk melihat detail informasi mengenai device driver psmouse
Menu 2 untuk memodifikasi value dari parameter rate
Menu 3 untuk memodifikasi value dari parameter resolution
Menu 4 untuk keluar dari script
