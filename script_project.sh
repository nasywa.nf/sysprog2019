#!/bin/bash

check_driver() {
echo "
=============================
        PSMOUSE STATUS
=============================" 
systool -vm psmouse
}

enterToContinue(){
read -p "Press [Enter] key to continue..."
}

modify_rate() {
echo -ne "
=============================
     MODIFY PSMOUSE RATE
=============================
Current psmouse rate: " 
check_driver | grep "rate" | awk '{print $3}' | tr -d '"'

echo -ne "
Set parameter rate to: "
        read user_input
	if ! [[ "$user_input" =~ ^[0-9]+$ ]] ; 
 		then exec >&2; echo "Error: Only digits are allowed"; exit 1
		else sudo modprobe -r psmouse; sudo modprobe psmouse rate=$user_input; echo "psmouse rate successfully updated"
	fi
}

modify_resolution() {
echo -ne "
=============================
  MODIFY PSMOUSE RESOLUTION
=============================
Current psmouse resolution: " 
check_driver | grep "resolution" | awk '{print $3}' | tr -d '"'

echo -ne "
Set parameter resolution to: "
        read user_input
	if ! [[ "$user_input" =~ ^[0-9]+$ ]] ; 
 		then exec >&2; echo "Error: Only digits are allowed"; exit 1
		else sudo modprobe -r psmouse; sudo modprobe psmouse resolution=$user_input; echo "psmouse resolution successfully updated"
	fi
}

menu(){
echo -ne "
============================
        MAIN MENU
============================
1. Check Device Driver (psmouse) Status
2. Modify psmouse rate
3. Modify psmouse resolution
4. Exit
Choose 1-4: "
        read a
        case $a in
                1) check_driver ; enterToContinue ; menu ;;
                2) modify_rate ; enterToContinue ; menu ;;
                3) modify_resolution ; enterToContinue ; menu ;;
                4) echo "Goodbye~" ; exit 0 ;;
                *) echo "Wrong option." ; WrongCommand;;
        esac
}

install_dependency() {
sudo apt install sysfsutils
}

# Start Script
install_dependency
menu
